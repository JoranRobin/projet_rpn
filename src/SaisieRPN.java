import java.util.EmptyStackException;
import java.util.Scanner;


public class SaisieRPN {

	public static final double MIN_VALUE = 0.0;
	public static final double MAX_VALUE = 99999.0;
	
	private Scanner sc ;
	private MoteurRPN moteur;
	
	public SaisieRPN()
	{
		moteur= new MoteurRPN();
		sc = new Scanner(System.in);
	}

public void saisieUser()
	{
		System.out.println("Saisir une opérande:");
		char op;
		String s=sc.nextLine();
		double saisie;
		
			
			while(!s.equals("exit"))
			{	
				try
				{
					op = s.charAt(0);
					if(op == '+')
					{
						moteur.app_operation(Operations.PLUS);
					}
					
					else if(op == '-')
					{
						moteur.app_operation(Operations.MOINS);
					}
					
					else if(op == '*')
					{
						moteur.app_operation(Operations.MULT);
					}
					
					else if (op == '/')
					{
						moteur.app_operation(Operations.DIV);
					}
					
					else 
					{
						saisie = Double.parseDouble(s);
						if(saisie > MAX_VALUE || saisie < MIN_VALUE)
						{
							throw new ExceptionValeur();
						}
					
						moteur.enregistrer(saisie);
					}
					
					moteur.toString();
				}
				
				catch(IllegalArgumentException e)
				{
					moteur.toString();
					System.out.println("Saisir un caractere valide : ");
				}
				
				catch(ExceptionOperandeUnique e)
				{
					System.out.println(e.getMessage());
					moteur.toString();
					System.out.println("Saisir un caractere valide : ");
				}
				
				catch(PileVide e)
				{
					System.out.println(e.getMessage());	
					moteur.toString();
					System.out.println("Saisir un caractère valide:");
				}
				
				catch(DivparZero e)
				{
					System.out.println(e.getMessage());
					moteur.toString();
					System.out.println("Saisir un caractère valide:");
				}
				
				catch(StringIndexOutOfBoundsException e)
				{
					System.out.println("Vous êtes Hors Des Limites");
					moteur.toString();
					System.out.println("Saisir un caractère valide:");
				}
				
				catch(ExceptionValeur e)
				{
					System.out.println(e.getMessage());
					moteur.toString();
					System.out.println("Saisir un caractère valide:");
				}
				
				s = sc.nextLine();
			}
			
			sc.close();
			System.exit(0);
		}

}
