
public class ExceptionOperandeUnique extends RuntimeException {
	
	public ExceptionOperandeUnique()
	{
		super("On ne peut pas appliquer une opération avec une seule opérande");
	}

}
