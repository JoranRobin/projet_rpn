import static org.junit.Assert.*;

public class Test {
	
	@org.junit.Test
	public void TestInstanceOpPlus()
	{
		Operations plus = Operations.PLUS;
		
		
		assertNotNull(plus);
		
	}
	@org.junit.Test
	public void TestInstanceOpMoins()
	{
		Operations moins = Operations.MOINS;
		
		assertNotNull(moins);
	}
	
	@org.junit.Test
	public void TestInstanceOpMult()
	{

		Operations mult = Operations.MULT;

		assertNotNull(mult);

	}
	
	@org.junit.Test
	public void TestInstanceOpDiv()
	{
		Operations div = Operations.DIV;
		
		assertNotNull(div);
	}

	@org.junit.Test
	public void TestEvalPlus() 
	{
		double p1 = 3;
		double p2 = 4;
		
		Operations plus = Operations.PLUS;
		
		assertEquals(7.0,plus.eval(p1,p2),0);		

	}
	
	@org.junit.Test
	public void TestEvalMoins() 
	{	
		double m1 = 4;
		double m2 = 3;

		Operations moins = Operations.MOINS;
		
		assertEquals(1,moins.eval(m1,m2),0);		

	}
	
	@org.junit.Test
	public void TestEvalMult() 
	{	
		double mu1 = 4;
		double mu2 = 3;
	
		Operations mult = Operations.MULT;
			
		assertEquals(12,mult.eval(mu1,mu2),0);		

	}
	
	@org.junit.Test
	public void TestEvalDiv() 
	{	
		double d1 = 3;
		double d2 = 4;
		
		Operations div = Operations.DIV;
		
		assertEquals(0.75,div.eval(d1,d2),0);		

	}

	@org.junit.Test
	public void TestInstanceMoteurRPN() 
	{	
		MoteurRPN m = new MoteurRPN();
		
		assertNotNull(m);		
	}
	
	@org.junit.Test
	public void TestEnregistrementPile() 
	{	
		MoteurRPN m = new MoteurRPN();
		m.enregistrer(2);
		
		assertEquals("[2.0]",m.toString());	
		assertEquals(1.0,m.getP().size(),0);
	}
	
	@org.junit.Test
	public void TestAffichagePileVide() 
	{	
		MoteurRPN m = new MoteurRPN();
		
		assertEquals("[]",m.toString());		
	}
	
	@org.junit.Test
	public void TestTaillePileVide() 
	{	
		MoteurRPN m = new MoteurRPN();

		assertEquals(0.0,m.getP().size(),0);		
	}

	@org.junit.Test
	public void TestAppOppPlus()
	{	
		double a=3;
		double b=4;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.PLUS);
		
		assertEquals("[7.0]",m.toString());			
	}
	
	@org.junit.Test
	public void TestAppOppMoins()
	{	
		double a=4;
		double b=3;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.MOINS);
		
		assertEquals("[1.0]",m.toString());			
	}
	

	@org.junit.Test
	public void TestInstanceSaisie()
	{
		SaisieRPN s = new SaisieRPN();
		
		assertNotNull(s);
	}
	
	@org.junit.Test
	public void TestAppOppMoinsValMin()
	{	
		try
		{
		double a=3;
		double b=4;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.MOINS);
		
		}
		
		catch(ExceptionValeur e)
		{
			assertTrue(e.getMessage().equals("Valeur Non Prise En Compte"));
		}
	}
	
	@org.junit.Test
	public void TestAppOppValMax()
	{	
		try
		{
		double a=1000000;
		double b=4;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.PLUS);
		
		}
		
		catch(ExceptionValeur e)
		{
			assertTrue(e.getMessage().equals("Valeur Non Prise En Compte"));
		}
	}
	
	
	@org.junit.Test
	public void TestAppOppMult()
	{	
		double a=3;
		double b=4;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.MULT);
		
		assertEquals("[12.0]",m.toString());			
	}
	
	@org.junit.Test
	public void TestInstanceCalculatrice()
	{
		CalculatriceRPN c = CalculatriceRPN.TEST;
		
		assertNotNull(c);

	}
	
	@org.junit.Test
	public void TestAppOppDiv()
	{	
		double a=3;
		double b=4;
		
		MoteurRPN m = new MoteurRPN();
		
		m.enregistrer(a);
		m.enregistrer(b);
		
		m.app_operation(Operations.DIV);
		
		assertEquals("[0.75]",m.toString());			
	}
	
	@org.junit.Test
	public void TestAppOppUnElement()
	{	
		try
		{
			double a=3;
			
			MoteurRPN m = new MoteurRPN();
			
			m.enregistrer(a);
			
			m.app_operation(Operations.PLUS);
		}
		
		catch(ExceptionOperandeUnique e)
		{
			assert e.getMessage().equals("On ne peut pas appliquer une opération avec une seule opérande");
		}
		
	}
	
	@org.junit.Test
	public void TestExceptionPileVide()
	{
		try
		{	
			MoteurRPN m = new MoteurRPN();
			
			m.app_operation(Operations.PLUS);
		}
		catch(PileVide e)
		{
			assertTrue(e.getMessage().equals("Pile Vide"));
		}	
	}
	
	@org.junit.Test
	public void TestDivParZero()
	{
		try
		{	
			double a = 1;
			double b = 0;
			
			MoteurRPN m = new MoteurRPN();
			
			m.enregistrer(a);
			m.enregistrer(b);
			
			m.app_operation(Operations.DIV);
		}
		catch(DivparZero e)
		{
			assertTrue(e.getMessage().equals("Division Par Zéro"));
		}	
	}
}
