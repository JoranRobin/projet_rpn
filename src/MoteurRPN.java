import java.util.*;

public class MoteurRPN {

	 private Stack<Double> p ;
	 
	 public MoteurRPN()
	 {
		 p= new Stack<Double> ();
	 }

	public void enregistrer(double operande) 
	 {
		 p.push(operande);
	 }
	
	 public void app_operation(Operations o)
	 {
		 if(p.size() >= 2)
		  {
			  double op2 = p.pop();
			  double op1 = p.pop();
			  
			  if(o.eval(op1,op2) >= SaisieRPN.MIN_VALUE && o.eval(op1,op2) <= SaisieRPN.MAX_VALUE)
			  {
					 enregistrer(o.eval(op1,op2));
			  }
			  
			  else if( o.getSymbole() == '/' && op2 == 0)
			  {
				  enregistrer(op1);
				  enregistrer(op2);
				  throw new DivparZero();
			  }
			  
			  else
			  {
				  enregistrer(op1);
				  enregistrer(op2);
				  throw new ExceptionValeur();
			  }		  
		  }
		 
		  else if(p.size() == 1)
		  {
			 throw new ExceptionOperandeUnique();
		  }  
		 
		  else
		  {
			  throw new PileVide();
		  }
	  }
	
	 public String toString()
	 {
		  String s = p.toString();
		  System.out.println(s);
		  return s;
	 }
	 
	 public Stack<Double> getP() 
	 {
			return p;
	 }
}
