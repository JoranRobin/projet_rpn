
public enum Operations {

	PLUS('+')
	 {
		@Override
		public double eval(double x, double y) 
		{
			return x+y;
		}	 
	 },
	 
	 MOINS('-')
	 {
		@Override
		public double eval(double x, double y) 
		{
			return x-y;
		}
	 },
	 
	 MULT('*')
	 {
		@Override
		public double eval(double x, double y) 
		{	
			return x*y ;
		}
	 },
	 
	DIV('/')
	{
		@Override
		public double eval(double x, double y)
		{		
				return x/y;
		}
	};
				
	private char symbole;
	 
	private Operations(char s)
	{
		symbole=s;
	}
	
	public abstract double eval(double x, double y);
	
	public char getSymbole()
	{
		return symbole;
	}
};